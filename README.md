# Descomplicando o Gitlab

Treinamento LinuxTips - Descomplicando o GitLab

### Day-1
```bash
- Entendemos o que é o Git
- Entendemos o que é Working Dir, Index e HEAD
- Entendemos o que é o Gitlab
- Como criar um Grupo no Gitlab
- Como criar um repositório do Git
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um membro no projeto
- Como fazer o Merge na Master/Main
- Como associar um repo local com um repo remoto
- Como importar um repo do GitHub para o Gitlab
- Mudamos a Branch padrão para Main
```
